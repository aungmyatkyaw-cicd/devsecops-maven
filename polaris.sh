#!/bin/bash
set -e
echo "Running the Polaris scan"
curl -o ./polaris_cli.zip -LO ${POLARIS_DOWNLOAD_URL}
unzip -d ./polaris_cli -j ./polaris_cli.zip
chmod +x ./polaris_cli/polaris
echo "$POLARIS_ACCESS_TOKEN" > POLARIS_ACCESS_TOKEN_FILE
[ -f "pom.xml" ] && ./polaris_cli/polaris -s https://${POLARIS_URL} --co capture.build.cleanCommands='[{"shell":["mvn","-B","-s","${CI_PROJECT_DIR}/settings.xml","-f","pom.xml","clean"]}]' --co capture.build.buildCommands='[{"shell":["mvn","-B","-s","${CI_PROJECT_DIR}/settings.xml","-f","pom.xml","install"]}]' analyze --coverity-ignore-capture-failure -w || ./polaris_cli/polaris -s https://${POLARIS_URL} analyze -w;
cat .synopsys/polaris/cli-scan.json
PL_PRJ_ID=$(cat .synopsys/polaris/cli-scan.json | jq '.projectInfo.projectId'| cut -d '"' -f 2)
PL_ACCESS_TOKEN=$(curl -s -X POST https://${POLARIS_URL}/api/auth/v1/authenticate -H 'Accept:application/json' -H 'Content-Type:application/x-www-form-urlencoded' -d accesstoken=$POLARIS_ACCESS_TOKEN | jq -r .jwt)
CRITICAL_STATUS=$(cat .synopsys/polaris/cli-scan.json | jq -r '.issueSummary.issuesBySeverity.critical')
HIGH_STATUS=$(cat .synopsys/polaris/cli-scan.json | jq -r '.issueSummary.issuesBySeverity.high')
MEDIUM_STATUS=$(cat .synopsys/polaris/cli-scan.json | jq -r '.issueSummary.issuesBySeverity.medium')
LOW_STATUS=$(cat .synopsys/polaris/cli-scan.json | jq -r '.issueSummary.issuesBySeverity.low')
POLARIS_PROJECT_URL=$(cat .synopsys/polaris/cli-scan.json | jq -r '.issueSummary.summaryUrl')
POLARIS_branchId=$(cat .synopsys/polaris/cli-scan.json | jq -r '.projectInfo.branchId')
echo "Generating Report PDF..."
curl -k -X GET -H "Host:${POLARIS_URL}" -b "access_token=${PL_ACCESS_TOKEN}" "https://${POLARIS_URL}/reporting/rs/v1/api/rs/reports/issues-report/export/?filter%5Bissue%5D%5Bstatus%5D%5B%24eq%5D=opened&format=pdf&include-source=true&project-id=${PL_PRJ_ID}&branch-id=${POLARIS_branchId}" -o report.pdf;
echo -e "**********************************************\nVulnerability Summary\nCritical Status: $CRITICAL_STATUS\nHigh Status: $HIGH_STATUS\nMedium Status: $MEDIUM_STATUS\nLow Status: $LOW_STATUS\n**********************************************";
if [[ "$CRITICAL_STATUS" -ne 0 ]]; then
    echo "Pipeline failed.";
    curl -X POST -H "Content-Type:application/json" -d '{"text":"<b style=color:red>Security Test(SAST) is FAILED due to '${CRITICAL_STATUS}' Critical Vulnerability!</b><br>Project Title: '${CI_PROJECT_NAME}' <br>Project URL: '${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}'<br>Polaris Project URL: '${POLARIS_PROJECT_URL}'<br><br><b>Vulnerability Details</b><br>Critical Status: '$CRITICAL_STATUS'<br>High Status: '$HIGH_STATUS'<br>Medium Status: '$MEDIUM_STATUS'<br>Low Status: '$LOW_STATUS'" }' ${TEAMS_WEBHOOK_URL};
    exit 1;
else
    echo "Pipeline succeeded.";
    curl -X POST -H "Content-Type:application/json" -d '{"text":"<b style=color:green>Security Test(SAST) is PASSED!</b><br>Project Title: '${CI_PROJECT_NAME}' <br>Project URL: '${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}'<br>Polaris Project URL: '${POLARIS_PROJECT_URL}'<br><br><b>Vulnerability Details</b><br>Critical Status: '$CRITICAL_STATUS'<br>High Status: '$HIGH_STATUS'<br>Medium Status: '$MEDIUM_STATUS'<br>Low Status: '$LOW_STATUS'" }' ${TEAMS_WEBHOOK_URL};
fi
