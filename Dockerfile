FROM registry.gitlab.com/aungmyatkyaw-cicd/base-images/maven
LABEL maintainer="Aung Myat Kyaw <aungmyatkyaw.kk@gmail.com>"
RUN apk --update add --no-cache unzip jq bash
COPY ./*.sh /opt/run/
