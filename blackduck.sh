#!/bin/bash
set -e
echo "Running the BlackDuck scan"
[ -f "pom.xml" ] && export BUILD_OPTIONS='--detect.maven.build.command="clean package -s ${CI_PROJECT_DIR}/settings.xml"';
bash <(curl -s -L ${HUB_SCRIPT_URL}) --blackduck.url="${HUB_URL}" --blackduck.api.token="${HUB_TOKEN}" ${BUILD_OPTIONS} --detect.debug=true --detect.risk.report.pdf=true --detect.risk.report.pdf.path=report | tee job.log
BD_PROJECT_ID=$(cat job.log | grep 'Black Duck Project' | awk -F 'BOM:' '{print $2}'  |  cut -d ' ' -f2 | awk -F '/components' '{print $1}'  | awk -F $HUB_URL/ '{print $2}')
BD_PROJECT_RP_ID=$(cat job.log | grep 'Black Duck Project' | awk -F 'BOM:' '{print $2}'  |  cut -d ' ' -f2 | awk -F '/components' '{print $1}')
BD_BEARER_TOKEN=$(curl -s -X POST $HUB_URL/api/tokens/authenticate -H "Accept:application/vnd.blackducksoftware.user-4+json" -H "Authorization:token $HUB_TOKEN" | jq -r .bearerToken)
BD_CRITICAL_STATUS=$(curl -s -X GET $HUB_URL/$BD_PROJECT_ID/risk-profile -b AUTHORIZATION_BEARER=$BD_BEARER_TOKEN | jq -r .categories.VULNERABILITY.CRITICAL)
BD_HIGH_STATUS=$(curl -s -X GET $HUB_URL/$BD_PROJECT_ID/risk-profile -b AUTHORIZATION_BEARER=$BD_BEARER_TOKEN | jq -r .categories.VULNERABILITY.HIGH)
BD_MEDIUM_STATUS=$(curl -s -X GET $HUB_URL/$BD_PROJECT_ID/risk-profile -b AUTHORIZATION_BEARER=$BD_BEARER_TOKEN | jq -r .categories.VULNERABILITY.MEDIUM)
BD_LOW_STATUS=$(curl -s -X GET $HUB_URL/$BD_PROJECT_ID/risk-profile -b AUTHORIZATION_BEARER=$BD_BEARER_TOKEN | jq -r .categories.VULNERABILITY.LOW)
echo -e "**********************************************\nVulnerability Summary\nCritical Status: $BD_CRITICAL_STATUS\nHigh Status: $BD_HIGH_STATUS\nMedium Status: $BD_MEDIUM_STATUS\nLow Status: $BD_LOW_STATUS\n**********************************************";
if [[ "$BD_CRITICAL_STATUS" -ne 0 ]]; then
    echo "Pipeline failed.";
    curl -X POST -H "Content-Type:application/json" -d '{"text":"<b style=color:red>Security Test(SCA) is FAILED due to '${BD_CRITICAL_STATUS}' Critical Vulnerability!</b><br>Project Title: '${CI_PROJECT_NAME}' <br>Project URL: '${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}' <br>Black Duck URL: '${BD_PROJECT_RP_ID}'/components<br><br><b>Vulnerability Details</b><br>Critical Status: '${BD_CRITICAL_STATUS}'<br>High Status: '${BD_HIGH_STATUS}'<br>Medium Status: '${BD_MEDIUM_STATUS}'<br>Low Status: '${BD_LOW_STATUS}'" }' ${TEAMS_WEBHOOK_URL};
    exit 1;
else
    echo "Pipeline succeeded.";
    curl -X POST -H "Content-Type:application/json" -d '{"text":"<b style=color:green>Security Test(SCA) is PASSED!</b><br>Project Title: '${CI_PROJECT_NAME}' <br>Project URL: '${CI_PROJECT_URL}/-/tree/${CI_COMMIT_REF_NAME}' <br>Black Duck URL: '${BD_PROJECT_RP_ID}'/components<br><br><p>Critical Status: '${BD_CRITICAL_STATUS}'<br>High Status: '${BD_HIGH_STATUS}'<br>Medium Status: '${BD_MEDIUM_STATUS}'<br>Low Status: '${BD_LOW_STATUS}'</p>" }' ${TEAMS_WEBHOOK_URL};
fi
